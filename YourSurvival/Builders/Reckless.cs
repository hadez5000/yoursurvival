﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourSurvival.Builders
{
    [System.Serializable]
    class Reckless : ICharacter
    {

       
        Random statsgen = new Random();

        public void statsgenerator()
        {
            agility = statsgen.Next(10, 15);
            Hp = statsgen.Next(20, 25);
            munitions = statsgen.Next(15, 20);
            Speech = statsgen.Next(10, 15);
            strenght = statsgen.Next(15, 20);
            gamestate = "";
            turns = 0;


        }
        public int turns { get; set; }
        public int agility { get; set; }
        public int Hp { get; set; }
        public int munitions { get; set; }
        public int Speech { get; set; }
        public int strenght { get; set; }
        public string name { get; set; }
        public string ClassSelected { get; set; }
        public string gamestate { get; set; }
    }
}
