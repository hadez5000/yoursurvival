﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourSurvival.Builders
{
    
    public interface ICharacter
    {
        int agility{get;set;}
        int Hp { get; set; }
        int munitions { get; set; }
        int Speech { get; set; }
        int strenght { get; set; }
        string name { get; set; }
        string ClassSelected { get; set; }
        string gamestate { get; set; }
        int turns { get; set; }
        



        void statsgenerator();
        
    }
}
