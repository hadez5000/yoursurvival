﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YourSurvival.Builders;


namespace YourSurvival
{
    
    public partial class MainMenu : Form
    {

        
        Createcharacter character = new Createcharacter();
        public MainMenu()
        {
            InitializeComponent();
            this.BackgroundImage = Properties.Resources.fallout_mainmenu;
        }

        public List<Player> charactercreation = new List<Player>();

        public void Charactercreate_Click(object sender, EventArgs e)
        {
            character = new Createcharacter();
            character.Show();
            if (Createcharacter.ActiveForm != null)
            {
                MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
                frm.WindowState = FormWindowState.Minimized;
                frm.ShowInTaskbar = true;
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("are you sure you want to exit the game?", "exit", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            Application.Exit();
        }

        public void Loadgame_Click(object sender, EventArgs e)
        {
            FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.Read);
            BinaryFormatter m_formatter = new BinaryFormatter();

            if (readerFileStream.Length != 0)
            {
                charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
            }
            readerFileStream.Close();

            if (charactercreation.Count == 0)
            {
                if (MessageBox.Show("Please create a character first.", "No savegames found.", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    return;
                }
            }

            Loadgame loadgames = new Loadgame();
            

            foreach (Player game in charactercreation)
            {

                loadgames.Loadergames.Items.Add( string.Format(
                    "{5} the {6}, " +
                    "HP: {0}, " +
                    "Strenght: {1}, " +
                    "Agility: {2}, " +
                    "Munitions: {3}, " +
                    "Speech: {4}",
                    game.character.Hp, game.character.strenght, game.character.agility, game.character.munitions, game.character.Speech, game.character.name, game.character.ClassSelected));

            }
            loadgames.Show();


            if (YourSurvival.Loadgame.ActiveForm != null)
            {
                MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
                frm.WindowState = FormWindowState.Minimized;
                frm.ShowInTaskbar = true;
            }
            
        }
        public string selectedgametext
        {
            get
            {
                return selectedgame.Text;
            }
            set
            {
                selectedgame.Text = value;
            }
        }

        private void Startgame_Click(object sender, EventArgs e)
        {
            
            Loadgame loadgames = new Loadgame();
            FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.Read);
            BinaryFormatter m_formatter = new BinaryFormatter();

            if (readerFileStream.Length == 0)
            {
                readerFileStream.Close();
            }

            charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
            readerFileStream.Close();

            foreach (var item in charactercreation)

                if (selectedgame.Text.Contains(item.character.name) && selectedgame.Text.Contains(item.character.ClassSelected))
                {
                    var charactergamestate = item;

                    Game realgame = new Game();
                    realgame.Text = string.Format("{0} the {1}", item.character.name, item.character.ClassSelected);
                    realgame.HPbox.Text = item.character.Hp.ToString() ;
                    realgame.Strenghtbox.Text = item.character.strenght.ToString();
                    realgame.Speechbox.Text = item.character.Speech.ToString();
                    realgame.Agilitybox.Text = item.character.agility.ToString();
                    realgame.Munitionsbox.Text = item.character.munitions.ToString();
                    realgame.namedeclarations.Text = string.Format("{0} the {1}", item.character.name, item.character.ClassSelected);
                    realgame.turnsleft.Text = string.Format((10 - item.character.turns).ToString());
                    realgame.teststringbox.Text = item.character.gamestate;

                        realgame.Show();
                    break;
                }
            if (YourSurvival.Game.ActiveForm != null)
            {
                MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
                frm.WindowState = FormWindowState.Minimized;
                frm.ShowInTaskbar = true;
            }
        }



        }
    }

