﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YourSurvival
{
    [Serializable]
    public partial class Game : Form
    {

        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        public string TestString = string.Empty;
        GameBrain brain = new GameBrain();
        public List<Player> charactercreation = new List<Player>();
        BinaryFormatter m_formatter = new BinaryFormatter();

        public Game()
        {

            InitializeComponent();
            FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
            readerFileStream.Close();

            brain.gamestate = string.Empty;
            turnsleft.Text = brain.turns.ToString();
            Talkerbox.Text = brain.talk;
            Opt1.Text = brain.but1;
            Opt2.Text = brain.but2;
            Opt3.Text = brain.but3;
            Opt4.Text = brain.but4;
            

        }

        public void updatestats()
        {
            if (HPbox.Text == string.Empty)
            {
                brain.gamestate = teststringbox.Text;
                brain.turns = int.Parse(turnsleft.Text);
                brain.hp = int.Parse(HPbox.Text);

                brain.strength = int.Parse(Strenghtbox.Text);
                brain.agility = int.Parse(Agilitybox.Text);
                brain.munitions = int.Parse(Munitionsbox.Text);
                brain.speech = int.Parse(Speechbox.Text);
            }
            else
            {
                brain.gamestate = TestString;
                HPbox.Text = brain.hp.ToString();
                Munitionsbox.Text = brain.munitions.ToString();
                brain.turns = int.Parse(turnsleft.Text);
            }
        }
        public void Checkgame()
        {
            if (brain.hp <= 0)
            {
                if (MessageBox.Show("You have lost, please try again.", "Game Over.", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    return;
                }
            }
            if (brain.turns == 1 && brain.hp > 0)
            {
                turnsleft.Text = "0";
                MessageBox.Show("Congratulations! You have survived for 10 days!", "You won!", MessageBoxButtons.OK);
            }
        }

        private void Closegame_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "Quit", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
                readerFileStream.Close();

                foreach (var item in charactercreation)
                {
                    if (namedeclarations.Text.Contains(item.character.name) && namedeclarations.Text.Contains(item.character.ClassSelected))
                    {
                        item.character.gamestate = brain.gamestate;
                        item.character.Hp = brain.hp;
                        item.character.munitions = brain.munitions;
                        item.character.turns = 10 - brain.turns;

                        FileStream RemoveOldAddNew = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        charactercreation.Remove(item);
                        charactercreation.Add(item);
                        m_formatter.Serialize(RemoveOldAddNew, charactercreation);
                        RemoveOldAddNew.Close();
                        break;
                    }
                }
                MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
                frm.WindowState = FormWindowState.Normal;
                Close();
            }
            else
            {
                return;
            }
        }

        private void Opt1_Click(object sender, EventArgs e)
        {
            TestString = teststringbox.Text;
            string button = "B1";
            TestString = string.Join(",", TestString, button);
            brain.gamestate = TestString;
            teststringbox.Text = TestString;
            brain.Gamechecker();
            updatestats();
            Checkgame();
            brain.turns--;
            turnsleft.Text = brain.turns.ToString();
        }

        private void Opt2_Click(object sender, EventArgs e)
        {
            TestString = teststringbox.Text;
            string button = "B2";
            TestString = string.Join(",", TestString, button);
            brain.gamestate = TestString;
            teststringbox.Text = TestString;
            brain.Gamechecker();
            updatestats();
            Checkgame();
            brain.turns--;
            turnsleft.Text = brain.turns.ToString();
        }
        private void Opt3_Click(object sender, EventArgs e)
        {
            TestString = teststringbox.Text;
            string button = "B3";
            TestString = string.Join(",", TestString, button);
            brain.gamestate = TestString;
            teststringbox.Text = TestString;
            brain.Gamechecker();
            updatestats();
            Checkgame();
            brain.turns--;
            turnsleft.Text = brain.turns.ToString();
        }

        private void Opt4_Click(object sender, EventArgs e)
        {
            TestString = teststringbox.Text;
            string button = "B4";
            TestString = string.Join(",", TestString, button);
            brain.gamestate = TestString;
            teststringbox.Text = TestString;
            brain.Gamechecker();
            updatestats();
            Checkgame();
            brain.turns--;
            turnsleft.Text = brain.turns.ToString();
        }
    }
}
