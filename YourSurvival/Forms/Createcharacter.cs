﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YourSurvival.Builders;

namespace YourSurvival
{
    [Serializable]
    public partial class Createcharacter : Form
    {

        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        public Createcharacter()
        {
            InitializeComponent();
            Characterclass.Items.Add("Gunner");
            Characterclass.Items.Add("Wiseman");
            Characterclass.Items.Add("Peacekeeper");
            Characterclass.Items.Add("The reckless");
            this.BackgroundImage = Properties.Resources.fallout_Charcreation;
            
        }

        public string charname { get; set; }
        public List<Player> charactercreation = new List<Player>();

        private const int WS_SYSMENU = 0x80000;


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }
        public void charnames()
        {
            charname = Charactername.Text;
        }


        public void Female_CheckedChanged(object sender, EventArgs e)
        {
            if (Female.Checked)
            {
                Male.Checked = false;
            }
            
        }

        public void Male_CheckedChanged(object sender, EventArgs e)
        {
            if (Male.Checked)
            {
                Female.Checked = false;
            }
        }

        public void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (Characterclass.Text == "Gunner")
            {
                sp.Stop();
                sp = new System.Media.SoundPlayer(Properties.Resources.Gunnersound);
                sp.Play();
                classDescription.Text = "Once an old veteran, now an expert with the gun. \r\n" +
                    "The gunner gains bonusses to Ammunitions and Agility";
                BackgroundImage = Properties.Resources.Gunner;
                
            }

            else if (Characterclass.Text == "Wiseman")
            {
                
                sp = new System.Media.SoundPlayer(Properties.Resources.Wisemansound);
                sp.Play();
                classDescription.Text = "Once the wiseman was a ruler, adored by everyone. Now tries to survive by ordering others. \r\n" +
                    "The wiseman gains bonusses to speech";
                this.BackgroundImage = Properties.Resources.Wiseman;
            }

            else if (Characterclass.Text == "Peacekeeper")
            {
                sp = new System.Media.SoundPlayer(Properties.Resources.Peacekeepersound);
                sp.Play();
                classDescription.Text = "Before the war the peacekeeper was a policeman, trying to keep the peace in the town. Now trying to maintain order in his group. \r\n" +
                    "The peacekeeper gains bonusses to speech and Ammunitions";
                this.BackgroundImage = Properties.Resources.Peacekeeper;
            }

            else if (Characterclass.Text == "The reckless")
            {
                sp = new System.Media.SoundPlayer(Properties.Resources.Recklesssound);
                sp.Play();
                classDescription.Text = "The reckles was a pitfighter, known for his brutal strenght. Now a force to be reckoned with. \r\n" +
                    "The reckless gains bonusses to HP and strenght.";
                this.BackgroundImage = Properties.Resources.reckless;
            }
        }

        public void Cancelcharacter_Click(object sender, EventArgs e)
        {
           
            MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
            frm.WindowState = FormWindowState.Normal;
            Close();
        }

        public void Charsave_Click(object sender, EventArgs e)
        {
            FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.Read);
            BinaryFormatter m_formatter = new BinaryFormatter();

            if (readerFileStream.Length == 0)
            {
                readerFileStream.Close();
            }
            else
            {
                charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
                readerFileStream.Close();
            }
            
            FileStream readerOrwriterFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            if (Characterclass.Text == "Gunner")
            {
                
                Player character = new Player(new Gunner(), Charactername.Text, "Gunner");
                int strtohp = character.character.strenght / 2 + 1 ;
                character.character.Hp += strtohp;
                charactercreation.Add(character);
                m_formatter.Serialize(readerOrwriterFileStream, charactercreation);
            }
            else if (Characterclass.Text == "Wiseman")
            {
                Player character = new Player(new Wiseman(), Charactername.Text, "Wiseman");
                int strtohp = character.character.strenght / 2 + 1;
                character.character.Hp += strtohp;
                charactercreation.Add(character);
                m_formatter.Serialize(readerOrwriterFileStream, charactercreation);
            }
            else if (Characterclass.Text == "Peacekeeper")
            {
                Player character = new Player(new Peacekeeper(), Charactername.Text, "Peacekeeper");
                int strtohp = character.character.strenght / 2 + 1;
                character.character.Hp += strtohp;
                charactercreation.Add(character);
                m_formatter.Serialize(readerOrwriterFileStream, charactercreation);
            }
            else if (Characterclass.Text == "The reckless")
            {
                Player character = new Player(new Reckless(), Charactername.Text, "Reckless");
                int strtohp = character.character.strenght / 2 + 1;
                character.character.Hp += strtohp;
                charactercreation.Add(character);
                m_formatter.Serialize(readerOrwriterFileStream, charactercreation);
            }
            readerOrwriterFileStream.Close();
            MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
            frm.WindowState = FormWindowState.Normal;
            Close();
        }
    }
}

          


