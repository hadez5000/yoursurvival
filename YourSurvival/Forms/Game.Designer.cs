﻿namespace YourSurvival
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.Opt1 = new System.Windows.Forms.Button();
            this.Opt2 = new System.Windows.Forms.Button();
            this.Opt3 = new System.Windows.Forms.Button();
            this.Opt4 = new System.Windows.Forms.Button();
            this.Talkerbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Speechbox = new System.Windows.Forms.TextBox();
            this.Agilitybox = new System.Windows.Forms.TextBox();
            this.Munitionsbox = new System.Windows.Forms.TextBox();
            this.Strenghtbox = new System.Windows.Forms.TextBox();
            this.HPbox = new System.Windows.Forms.TextBox();
            this.Closegame = new System.Windows.Forms.Button();
            this.turnsleft = new System.Windows.Forms.TextBox();
            this.namedeclarations = new System.Windows.Forms.TextBox();
            this.turnos = new System.Windows.Forms.Label();
            this.teststringbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Opt1
            // 
            this.Opt1.Location = new System.Drawing.Point(637, 12);
            this.Opt1.Name = "Opt1";
            this.Opt1.Size = new System.Drawing.Size(252, 121);
            this.Opt1.TabIndex = 0;
            this.Opt1.UseVisualStyleBackColor = true;
            this.Opt1.Click += new System.EventHandler(this.Opt1_Click);
            // 
            // Opt2
            // 
            this.Opt2.Location = new System.Drawing.Point(637, 139);
            this.Opt2.Name = "Opt2";
            this.Opt2.Size = new System.Drawing.Size(252, 121);
            this.Opt2.TabIndex = 1;
            this.Opt2.UseVisualStyleBackColor = true;
            this.Opt2.Click += new System.EventHandler(this.Opt2_Click);
            // 
            // Opt3
            // 
            this.Opt3.Location = new System.Drawing.Point(637, 266);
            this.Opt3.Name = "Opt3";
            this.Opt3.Size = new System.Drawing.Size(252, 121);
            this.Opt3.TabIndex = 2;
            this.Opt3.UseVisualStyleBackColor = true;
            this.Opt3.Click += new System.EventHandler(this.Opt3_Click);
            // 
            // Opt4
            // 
            this.Opt4.Location = new System.Drawing.Point(637, 393);
            this.Opt4.Name = "Opt4";
            this.Opt4.Size = new System.Drawing.Size(252, 121);
            this.Opt4.TabIndex = 3;
            this.Opt4.UseVisualStyleBackColor = true;
            this.Opt4.Click += new System.EventHandler(this.Opt4_Click);
            // 
            // Talkerbox
            // 
            this.Talkerbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Talkerbox.Location = new System.Drawing.Point(12, 12);
            this.Talkerbox.Multiline = true;
            this.Talkerbox.Name = "Talkerbox";
            this.Talkerbox.ReadOnly = true;
            this.Talkerbox.Size = new System.Drawing.Size(496, 147);
            this.Talkerbox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Hp";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Strength";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Speech";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Agility";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 272);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Munitions";
            // 
            // Speechbox
            // 
            this.Speechbox.Location = new System.Drawing.Point(63, 217);
            this.Speechbox.Name = "Speechbox";
            this.Speechbox.ReadOnly = true;
            this.Speechbox.Size = new System.Drawing.Size(100, 20);
            this.Speechbox.TabIndex = 10;
            // 
            // Agilitybox
            // 
            this.Agilitybox.Location = new System.Drawing.Point(63, 243);
            this.Agilitybox.Name = "Agilitybox";
            this.Agilitybox.ReadOnly = true;
            this.Agilitybox.Size = new System.Drawing.Size(100, 20);
            this.Agilitybox.TabIndex = 11;
            // 
            // Munitionsbox
            // 
            this.Munitionsbox.Location = new System.Drawing.Point(63, 269);
            this.Munitionsbox.Name = "Munitionsbox";
            this.Munitionsbox.ReadOnly = true;
            this.Munitionsbox.Size = new System.Drawing.Size(100, 20);
            this.Munitionsbox.TabIndex = 12;
            // 
            // Strenghtbox
            // 
            this.Strenghtbox.Location = new System.Drawing.Point(63, 191);
            this.Strenghtbox.Name = "Strenghtbox";
            this.Strenghtbox.ReadOnly = true;
            this.Strenghtbox.Size = new System.Drawing.Size(100, 20);
            this.Strenghtbox.TabIndex = 13;
            // 
            // HPbox
            // 
            this.HPbox.Location = new System.Drawing.Point(63, 165);
            this.HPbox.Name = "HPbox";
            this.HPbox.ReadOnly = true;
            this.HPbox.Size = new System.Drawing.Size(100, 20);
            this.HPbox.TabIndex = 14;
            // 
            // Closegame
            // 
            this.Closegame.Location = new System.Drawing.Point(12, 470);
            this.Closegame.Name = "Closegame";
            this.Closegame.Size = new System.Drawing.Size(193, 44);
            this.Closegame.TabIndex = 15;
            this.Closegame.Text = "Exit and return to main menu";
            this.Closegame.UseVisualStyleBackColor = true;
            this.Closegame.Click += new System.EventHandler(this.Closegame_Click);
            // 
            // turnsleft
            // 
            this.turnsleft.Location = new System.Drawing.Point(63, 295);
            this.turnsleft.Name = "turnsleft";
            this.turnsleft.ReadOnly = true;
            this.turnsleft.Size = new System.Drawing.Size(100, 20);
            this.turnsleft.TabIndex = 16;
            // 
            // namedeclarations
            // 
            this.namedeclarations.Location = new System.Drawing.Point(420, 494);
            this.namedeclarations.Name = "namedeclarations";
            this.namedeclarations.ReadOnly = true;
            this.namedeclarations.Size = new System.Drawing.Size(100, 20);
            this.namedeclarations.TabIndex = 17;
            this.namedeclarations.Visible = false;
            // 
            // turnos
            // 
            this.turnos.AutoSize = true;
            this.turnos.Location = new System.Drawing.Point(2, 298);
            this.turnos.Name = "turnos";
            this.turnos.Size = new System.Drawing.Size(55, 13);
            this.turnos.TabIndex = 18;
            this.turnos.Text = "Turns Left";
            // 
            // teststringbox
            // 
            this.teststringbox.Location = new System.Drawing.Point(250, 266);
            this.teststringbox.Name = "teststringbox";
            this.teststringbox.Size = new System.Drawing.Size(301, 20);
            this.teststringbox.TabIndex = 19;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(901, 521);
            this.Controls.Add(this.teststringbox);
            this.Controls.Add(this.turnos);
            this.Controls.Add(this.namedeclarations);
            this.Controls.Add(this.turnsleft);
            this.Controls.Add(this.Closegame);
            this.Controls.Add(this.HPbox);
            this.Controls.Add(this.Strenghtbox);
            this.Controls.Add(this.Munitionsbox);
            this.Controls.Add(this.Agilitybox);
            this.Controls.Add(this.Speechbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Talkerbox);
            this.Controls.Add(this.Opt4);
            this.Controls.Add(this.Opt3);
            this.Controls.Add(this.Opt2);
            this.Controls.Add(this.Opt1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(917, 560);
            this.Name = "Game";
            this.Text = "Game";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox Talkerbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox Speechbox;
        public System.Windows.Forms.TextBox Agilitybox;
        public System.Windows.Forms.TextBox Munitionsbox;
        public System.Windows.Forms.TextBox Strenghtbox;
        public System.Windows.Forms.TextBox HPbox;
        private System.Windows.Forms.Button Closegame;
        public System.Windows.Forms.TextBox namedeclarations;
        private System.Windows.Forms.Label turnos;
        public System.Windows.Forms.TextBox turnsleft;
        public System.Windows.Forms.Button Opt1;
        public System.Windows.Forms.Button Opt2;
        public System.Windows.Forms.Button Opt3;
        public System.Windows.Forms.Button Opt4;
        public System.Windows.Forms.TextBox teststringbox;
    }
}