﻿namespace YourSurvival
{
    partial class Createcharacter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Createcharacter));
            this.Characterclass = new System.Windows.Forms.ComboBox();
            this.Male = new System.Windows.Forms.RadioButton();
            this.Female = new System.Windows.Forms.RadioButton();
            this.classDescription = new System.Windows.Forms.TextBox();
            this.Charname = new System.Windows.Forms.Label();
            this.Charclass = new System.Windows.Forms.Label();
            this.Charsave = new System.Windows.Forms.Button();
            this.Cancelcharacter = new System.Windows.Forms.Button();
            this.Charactername = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Characterclass
            // 
            this.Characterclass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Characterclass.FormattingEnabled = true;
            this.Characterclass.Location = new System.Drawing.Point(177, 38);
            this.Characterclass.Name = "Characterclass";
            this.Characterclass.Size = new System.Drawing.Size(145, 21);
            this.Characterclass.TabIndex = 0;
            this.Characterclass.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Male
            // 
            this.Male.AutoSize = true;
            this.Male.BackColor = System.Drawing.Color.Transparent;
            this.Male.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Male.ForeColor = System.Drawing.Color.Maroon;
            this.Male.Location = new System.Drawing.Point(264, 79);
            this.Male.Name = "Male";
            this.Male.Size = new System.Drawing.Size(47, 17);
            this.Male.TabIndex = 2;
            this.Male.TabStop = true;
            this.Male.Text = "Male";
            this.Male.UseVisualStyleBackColor = false;
            this.Male.CheckedChanged += new System.EventHandler(this.Male_CheckedChanged);
            // 
            // Female
            // 
            this.Female.AutoSize = true;
            this.Female.BackColor = System.Drawing.Color.Transparent;
            this.Female.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Female.ForeColor = System.Drawing.Color.Maroon;
            this.Female.Location = new System.Drawing.Point(264, 102);
            this.Female.Name = "Female";
            this.Female.Size = new System.Drawing.Size(58, 17);
            this.Female.TabIndex = 3;
            this.Female.TabStop = true;
            this.Female.Text = "Female";
            this.Female.UseVisualStyleBackColor = false;
            this.Female.CheckedChanged += new System.EventHandler(this.Female_CheckedChanged);
            // 
            // classDescription
            // 
            this.classDescription.Location = new System.Drawing.Point(12, 210);
            this.classDescription.Multiline = true;
            this.classDescription.Name = "classDescription";
            this.classDescription.Size = new System.Drawing.Size(310, 60);
            this.classDescription.TabIndex = 4;
            // 
            // Charname
            // 
            this.Charname.AutoSize = true;
            this.Charname.BackColor = System.Drawing.Color.Transparent;
            this.Charname.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Charname.ForeColor = System.Drawing.Color.DarkRed;
            this.Charname.Location = new System.Drawing.Point(89, 15);
            this.Charname.Name = "Charname";
            this.Charname.Size = new System.Drawing.Size(82, 13);
            this.Charname.TabIndex = 5;
            this.Charname.Text = "Character name";
            // 
            // Charclass
            // 
            this.Charclass.AutoSize = true;
            this.Charclass.BackColor = System.Drawing.Color.Transparent;
            this.Charclass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Charclass.ForeColor = System.Drawing.Color.DarkRed;
            this.Charclass.Location = new System.Drawing.Point(89, 41);
            this.Charclass.Name = "Charclass";
            this.Charclass.Size = new System.Drawing.Size(80, 13);
            this.Charclass.TabIndex = 6;
            this.Charclass.Text = "Character class";
            // 
            // Charsave
            // 
            this.Charsave.Location = new System.Drawing.Point(189, 276);
            this.Charsave.Name = "Charsave";
            this.Charsave.Size = new System.Drawing.Size(133, 23);
            this.Charsave.TabIndex = 7;
            this.Charsave.Text = "Save";
            this.Charsave.UseVisualStyleBackColor = true;
            this.Charsave.Click += new System.EventHandler(this.Charsave_Click);
            // 
            // Cancelcharacter
            // 
            this.Cancelcharacter.Location = new System.Drawing.Point(12, 276);
            this.Cancelcharacter.Name = "Cancelcharacter";
            this.Cancelcharacter.Size = new System.Drawing.Size(133, 23);
            this.Cancelcharacter.TabIndex = 8;
            this.Cancelcharacter.Text = "Cancel";
            this.Cancelcharacter.UseVisualStyleBackColor = true;
            this.Cancelcharacter.Click += new System.EventHandler(this.Cancelcharacter_Click);
            // 
            // Charactername
            // 
            this.Charactername.Location = new System.Drawing.Point(177, 12);
            this.Charactername.Name = "Charactername";
            this.Charactername.Size = new System.Drawing.Size(145, 20);
            this.Charactername.TabIndex = 1;
            // 
            // Createcharacter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YourSurvival.Properties.Resources.fallout_Charcreation;
            this.ClientSize = new System.Drawing.Size(334, 311);
            this.Controls.Add(this.Cancelcharacter);
            this.Controls.Add(this.Charsave);
            this.Controls.Add(this.Charclass);
            this.Controls.Add(this.Charname);
            this.Controls.Add(this.classDescription);
            this.Controls.Add(this.Female);
            this.Controls.Add(this.Male);
            this.Controls.Add(this.Charactername);
            this.Controls.Add(this.Characterclass);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Createcharacter";
            this.Text = "Createcharacter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RadioButton Male;
        private System.Windows.Forms.TextBox classDescription;
        private System.Windows.Forms.Label Charname;
        private System.Windows.Forms.Label Charclass;
        public System.Windows.Forms.ComboBox Characterclass;
        public System.Windows.Forms.TextBox Charactername;
        public System.Windows.Forms.RadioButton Female;
        public System.Windows.Forms.Button Charsave;
        public System.Windows.Forms.Button Cancelcharacter;
    }
}