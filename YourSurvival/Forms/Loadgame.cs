﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YourSurvival
{
    public partial class Loadgame : Form
    {
        public Loadgame()
        {
            InitializeComponent();
            
        }
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        private void Loadstate_Click(object sender, EventArgs e)
        {

            MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
            frm.selectedgametext = Loadergames.Text;
                
            frm.WindowState = FormWindowState.Normal;
            Close();
        }

        public void DeleteSave_Click(object sender, EventArgs e)
        {
            List<Player> charactercreation = new List<Player>();

            FileStream readerFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.Open, FileAccess.Read);
            BinaryFormatter m_formatter = new BinaryFormatter();
            charactercreation = (List<Player>)m_formatter.Deserialize(readerFileStream);
            readerFileStream.Close();
            if (charactercreation.Count == 0)
            {
                MessageBox.Show("You can't delete a game which does not exist! You idiot.", "aawh :(", MessageBoxButtons.OK);
                return;
            }
            if (MessageBox.Show("Do you really want to Delete this Savegame?", "Delete savegame", 
                MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            else
            {
                if (Loadergames.SelectedIndex < 0)
                {
                    charactercreation.RemoveAt(Loadergames.TopIndex);
                }
                else
                {
                    charactercreation.RemoveAt(Loadergames.SelectedIndex);
                }
               
                FileStream WriterFileStream = new FileStream("D:/savedgame/savedgame.gb", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                m_formatter.Serialize(WriterFileStream, charactercreation);
                WriterFileStream.Close();

                Loadergames.Items.Clear();

                foreach(Player game in charactercreation)
                Loadergames.Items.Add(string.Format(
                    "{5} the {6}, " +
                    "HP: {0}, " +
                    "Strenght: {1}, " +
                    "Agility: {2}, " +
                    "Munitions: {3}, " +
                    "Speech: {4}",
                    game.character.Hp,
                    game.character.strenght,
                    game.character.agility,
                    game.character.munitions,
                    game.character.Speech,
                    game.character.name,
                    game.character.ClassSelected));

            }
           

        }
        
        private void Close_Click(object sender, EventArgs e)
        {
            MainMenu frm = (MainMenu)Application.OpenForms["MainMenu"];
            frm.WindowState = FormWindowState.Normal;
            Close();
        }
        
        private void Loadergames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Loadergames.SelectedIndex == -1)
            {
                return;
            }
            else if (Loadergames.SelectedItem.ToString().Contains("Gunner"))
            {
                pictureBox1.Image = Properties.Resources.Gunner_load;
            }
            else if (Loadergames.SelectedItem.ToString().Contains("Wiseman"))
            {
                pictureBox1.Image = Properties.Resources.Wiseman_load;
            }
            else if (Loadergames.SelectedItem.ToString().Contains("Peacekeeper"))
            {
                pictureBox1.Image = Properties.Resources.Peacekeeper_load;
            }
            else if (Loadergames.SelectedItem.ToString().Contains("Reckless"))
            {
                pictureBox1.Image = Properties.Resources.reckless_load;
            }
            //    textBox1.Text = Loadergames.Text;
            //if (textBox1.Text.Contains("Gunner"))

        }
    }
}
