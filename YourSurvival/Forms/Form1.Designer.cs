﻿namespace YourSurvival
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.Loadgame = new System.Windows.Forms.Button();
            this.Startgame = new System.Windows.Forms.Button();
            this.Charactercreate = new System.Windows.Forms.Button();
            this.Savegame = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.selectedgame = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Loadgame
            // 
            this.Loadgame.Location = new System.Drawing.Point(622, 295);
            this.Loadgame.Name = "Loadgame";
            this.Loadgame.Size = new System.Drawing.Size(150, 50);
            this.Loadgame.TabIndex = 2;
            this.Loadgame.Text = "Load game";
            this.Loadgame.UseVisualStyleBackColor = true;
            this.Loadgame.Click += new System.EventHandler(this.Loadgame_Click);
            // 
            // Startgame
            // 
            this.Startgame.Location = new System.Drawing.Point(12, 371);
            this.Startgame.Name = "Startgame";
            this.Startgame.Size = new System.Drawing.Size(150, 50);
            this.Startgame.TabIndex = 3;
            this.Startgame.Text = "Start game";
            this.Startgame.UseVisualStyleBackColor = true;
            this.Startgame.Click += new System.EventHandler(this.Startgame_Click);
            // 
            // Charactercreate
            // 
            this.Charactercreate.Location = new System.Drawing.Point(317, 371);
            this.Charactercreate.Name = "Charactercreate";
            this.Charactercreate.Size = new System.Drawing.Size(150, 50);
            this.Charactercreate.TabIndex = 4;
            this.Charactercreate.Text = "Create a character";
            this.Charactercreate.UseVisualStyleBackColor = true;
            this.Charactercreate.Click += new System.EventHandler(this.Charactercreate_Click);
            // 
            // Savegame
            // 
            this.Savegame.Location = new System.Drawing.Point(12, 295);
            this.Savegame.Name = "Savegame";
            this.Savegame.Size = new System.Drawing.Size(150, 50);
            this.Savegame.TabIndex = 5;
            this.Savegame.Text = "Save game";
            this.Savegame.UseVisualStyleBackColor = true;
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(622, 371);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(150, 50);
            this.Exit.TabIndex = 7;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // selectedgame
            // 
            this.selectedgame.Location = new System.Drawing.Point(12, 429);
            this.selectedgame.Name = "selectedgame";
            this.selectedgame.ReadOnly = true;
            this.selectedgame.Size = new System.Drawing.Size(150, 20);
            this.selectedgame.TabIndex = 8;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 461);
            this.ControlBox = false;
            this.Controls.Add(this.selectedgame);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Savegame);
            this.Controls.Add(this.Charactercreate);
            this.Controls.Add(this.Startgame);
            this.Controls.Add(this.Loadgame);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(815, 500);
            this.MinimizeBox = false;
            this.Name = "MainMenu";
            this.Text = "Main Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Loadgame;
        private System.Windows.Forms.Button Startgame;
        private System.Windows.Forms.Button Charactercreate;
        private System.Windows.Forms.Button Savegame;
        private System.Windows.Forms.Button Exit;
        public System.Windows.Forms.TextBox selectedgame;
    }
}

