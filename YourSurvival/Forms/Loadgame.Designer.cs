﻿namespace YourSurvival
{
    partial class Loadgame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Loadgame));
            this.Loadstate = new System.Windows.Forms.Button();
            this.DeleteSave = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.Loadergames = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Loadstate
            // 
            this.Loadstate.Location = new System.Drawing.Point(0, 246);
            this.Loadstate.Name = "Loadstate";
            this.Loadstate.Size = new System.Drawing.Size(284, 44);
            this.Loadstate.TabIndex = 1;
            this.Loadstate.Text = "Load gamestate";
            this.Loadstate.UseVisualStyleBackColor = true;
            this.Loadstate.Click += new System.EventHandler(this.Loadstate_Click);
            // 
            // DeleteSave
            // 
            this.DeleteSave.Location = new System.Drawing.Point(0, 196);
            this.DeleteSave.Name = "DeleteSave";
            this.DeleteSave.Size = new System.Drawing.Size(284, 44);
            this.DeleteSave.TabIndex = 2;
            this.DeleteSave.Text = "Delete savegame";
            this.DeleteSave.UseVisualStyleBackColor = true;
            this.DeleteSave.Click += new System.EventHandler(this.DeleteSave_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(0, 296);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(284, 44);
            this.Close.TabIndex = 3;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Loadergames
            // 
            this.Loadergames.FormattingEnabled = true;
            this.Loadergames.Location = new System.Drawing.Point(0, 12);
            this.Loadergames.Name = "Loadergames";
            this.Loadergames.Size = new System.Drawing.Size(470, 173);
            this.Loadergames.TabIndex = 4;
            this.Loadergames.SelectedIndexChanged += new System.EventHandler(this.Loadergames_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(290, 191);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 150);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Loadgame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 344);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Loadergames);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.DeleteSave);
            this.Controls.Add(this.Loadstate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Loadgame";
            this.Text = "Loadgame";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Loadstate;
        private System.Windows.Forms.Button DeleteSave;
        private System.Windows.Forms.Button Close;
        public System.Windows.Forms.ListBox Loadergames;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}