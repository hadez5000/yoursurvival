﻿using System.Threading;
using System.Threading.Tasks;

namespace YourSurvival
{
    public class GameBrain
    {
        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        string substring;
        public int hp { get; set; }
        public int strength { get; set; }
        public int agility { get; set; }
        public int munitions { get; set; }
        public int speech { get; set; }
        
        public int turns { get; set; }
        public string talk { get; set; }
        public string but1 { get; set; }
        public string but2 { get; set; }
        public string but3 { get; set; }
        public string but4 { get; set; }
        public string gamestate { get; set; }

        public void Gamechecker()
        {
            substring = gamestate.Substring(0, 3);
            if (substring == ",B1")
            {
                sp = new System.Media.SoundPlayer(Properties.Resources.crow_1);
                Task.Run(() =>
                {
                    while (true)
                    {
                        sp.Play();
                        Thread.Sleep(8000);
                    }
                });
                hp = 24;
                gamestate = gamestate.Remove(0, 3);
                if (gamestate != string.Empty)
                {
                    substring = gamestate.Substring(0, 3);
                    if (substring == ",B1")
                    {
                        hp = 24;
                        gamestate = gamestate.Remove(0, 3);
                        substring = gamestate.Substring(0, 3);

                    }
                }
            }
            else if (substring == ",B2")
            {
                hp = 24;
                gamestate = gamestate.Remove(0, 3);
                if (gamestate != string.Empty)
                {
                    substring = gamestate.Substring(0, 3);
                    if (substring == ",B1")
                    {
                        hp = 24;
                    }

                }

            }
            else if (substring == ",B3")
            {
                hp = 24;
                gamestate = gamestate.Remove(0, 3);
                substring = gamestate.Substring(0, 3);
                if (substring == ",B1")
                {
                    hp = 24;
                    gamestate = gamestate.Remove(0, 3);
                    substring = gamestate.Substring(0, 3);

                }


            }
            else if (substring == ",B4")
            {
                hp = 24;
                gamestate = gamestate.Remove(0, 3);
                substring = gamestate.Substring(0, 3);
                if (substring == ",B1")
                {
                    hp = 24;
                    gamestate = gamestate.Remove(0, 3);
                    substring = gamestate.Substring(0, 3);
                }
                gamestate = gamestate.Remove(0, 3);
            }
                

            
        }

    }
}
