﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YourSurvival.Builders;

namespace YourSurvival
{
    

    [System.Serializable]
    public class Player
    {
        TextInfo textinfo = new CultureInfo("en-US", true).TextInfo;
        private ICharacter _class;
        public Player(ICharacter Class, string name, string ClassSelected)
        {
            _class = Class;
            Class.name = textinfo.ToTitleCase(name) ;
            Class.ClassSelected = ClassSelected;
            statsgenerator();
            
        }

        public void statsgenerator()
        {
            _class.statsgenerator();
        }
        public ICharacter character {
            get
            {
                return _class;
            }
        }
    }
}
